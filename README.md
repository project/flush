## CONTENTS OF THIS FILE

 * Introduction
 * Installation
 * Configuration
 * Maintainers

## INTRODUCTION

This module plays a fun flushing sound when the cache is flushed. Sound is 
configurable in the admin settings under UI.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/flush/

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/flush

## INSTALLATION

 * Install module using `composer require drupal/flush`
 * Enable the Flush module.

## CONFIGURATION

 * There is a configuration link for this, which you can access at
   admin/config/system/flush-settings.

## MAINTAINERS

Current maintainers:
 * Jack Lampinen - https://www.drupal.org/u/jacklampinen
