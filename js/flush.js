/**
 * @file
 * Flush behaviors.
 */

(function ($, Drupal, once) {

  'use strict';

  /**
   * Triggers when the cache is cleared to play a sound.
   *
   * @type {Drupal~behavior}
   */

  Drupal.behaviors.flush = {
    attach: function (context, settings) {
      once('flush', context).forEach(function () {
        if (settings.flushCacheCleared === 'true') {
          const flush = document.createElement('audio');
          flush.setAttribute('src', settings.flush.pathToSound);
          flush.play();
        }
      });
    }
  };

}(jQuery, Drupal, once));
