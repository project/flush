<?php

namespace Drupal\flush\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Configure Flush settings for this site.
 */
class FlushSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'flush.settings';

  /**
   * Path to flush asset.
   *
   * @var string
   */
  const PATH = '/assets/flush.mp3';

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The current Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The file storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileUrlGeneratorInterface $file_url_generator, ModuleHandlerInterface $module_handler, Request $request, EntityStorageInterface $file_storage) {
    parent::__construct($config_factory);
    $this->fileUrlGenerator = $file_url_generator;
    $this->moduleHandler = $module_handler;
    $this->request = $request;
    $this->fileStorage = $file_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_url_generator'),
      $container->get('module_handler'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_type.manager')->getStorage('file')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'flush_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['pathToSound'] = [
      '#markup' => '<p> Current sound path: ' . $config->get('pathToSound') . '</p>',
    ];
    // Sets pathToSound to default sound that comes with Flush.
    $form['set_default'] = [
      '#type' => 'submit',
      '#value' => $this->t('Set to Default (Toilet Flush)'),
      '#submit' => [[$this, 'setToDefault']],
    ];
    $form['or'] = [
      '#markup' => '<p> or </p>',
    ];
    // Allows user to upload their own sound and sets pathToSound to that.
    $form['upload']['sound_dir'] = [
      '#type' => 'managed_file',
      '#upload_location' => 'public://',
      '#required' => FALSE,
      '#multiple' => FALSE,
      '#description' => $this->t('Allowed extensions: mp3 wav'),
      '#upload_validators' => [
        'file_validate_extensions' => ['mp3 wav'],
        'file_validate_size' => [25600000],
      ],
      '#title' => $this->t('Upload a sound file:'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file_data = $form_state->getValue(['upload' => 'sound_dir']);
    // If the submit button is pressed and the user actually uploaded an image,
    // upload it and set the url to its directory.
    if ($file_data) {
      /** @var \Drupal\file\Entity\File $file */
      $file = $this->fileStorage->load($file_data[0]);
      $uri = $file->getFileUri();
      $url = Url::fromUri($this->fileUrlGenerator->generateAbsoluteString($uri))->toString();
      $file->setPermanent();
      $file->save();

      $this->configFactory->getEditable(static::SETTINGS)
        ->set('pathToSound', $url)
        ->set('useDefault', FALSE)
        ->save();
      parent::submitForm($form, $form_state);
    }
  }

  /**
   * Updates default flush sound.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function setToDefault(array &$form, FormStateInterface $form_state) {
    $url = $this->getRequest()->getSchemeAndHttpHost();
    $url = $url . $this->request->getBaseUrl();
    $module_path = $this->moduleHandler->getModule('flush')->getPath();
    $url = $url . '/' . $module_path . static::PATH;

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('pathToSound', $url)
      ->set('useDefault', TRUE)
      ->save();
  }

}
